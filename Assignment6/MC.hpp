//
//  MC.h
//  
//
//  Created by Gu, Fangyuan on 29/01/2016.
//
//

#ifndef MC_h
#define MC_h

#include <cmath>
#include <cstdlib>
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

//structdef Ranq2
typedef int Int; // 32 bit integer
typedef unsigned int Uint;

typedef long long int Llong; // 64 bit integer
typedef unsigned long long int Ullong;

typedef double Doub; // default floating type
typedef long double Ldoub;

const double PI =3.141592653589793238463;

#endif /* MC_h */
