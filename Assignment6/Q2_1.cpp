#include"MC.hpp"

struct Ranq2 {
    //Random number generator which has long period and fast speed. The period is about 8.5*(10^37)
    Ullong v,w;
    Ranq2(Ullong j) : v(4101842887655102017LL), w(1) {
        v ^= j;
        w = int64();
        v = int64();
    }
    inline Ullong int64() {
        v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
        w = 4294957665U*(w & 0xffffffff) + (w >> 32);
        return v ^ w;
    }
    inline Doub doub() { return 5.42101086242752217E-20 * int64(); }
    //Return random double-precision floating value in the Range 0 to 1.
    inline Uint int32() { return (Uint)int64(); }
    //Return 32-bit random integar.
};

double F(double x){//define the function to integral
    return exp(-x*x);
}

double Accuracy(){//let user specify the accuracy for integral
    
    return 0.001;
}

int main(){
    double ran, x;
    double sum = 0.0;
    double avgValue[1000], ans[1000];
    int n=40;//Set the initial total number of random numbers.
    double e=Accuracy();//Set user defined accuracy:
    double b1=0,b2=2;//Set boundary for the integration
    Ranq2 f(9);
    for (int j = 1; j <= 2; j++)
    {
        for (int i = 1; i <= j*n; i++)
        {
        ran = f.doub();
        x = b1 + (b2-b1)*ran;
        sum += F(x);
        
        }
       avgValue[j] = sum/(j*n);
       ans[j] = (b2-b1)*avgValue[j];
       sum = 0.0;//restore sum
     }
        
     double err = fabs((ans[2]-ans[1])/ans[2]);
     //define the initial error for using 40 points
     int m = 2;
     n = 2*n;
     sum = 0.0;
    
     while(err>e){
     n = 2*n;
     m++;
     for (int i = 1; i <= n; i++)
        {
        ran = f.doub();
        x = b1 + (b2-b1)*ran;
        sum += F(x);

        }
      avgValue[m] = sum/n;
      ans[m] = (b2-b1)*avgValue[m];
      err = fabs((ans[m]-ans[m-1])/ans[m]);
      sum = 0.0;
      }
      
      double I = 0.5*(ans[m]+ ans[m-1]);
      cout<<"Total number of points used: "<<n<<endl;
      cout<<"The integral of f(x) between 0 and 2 is: "<<2/sqrt(PI)*I<<endl;
      }
        
     
        
        














