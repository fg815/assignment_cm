#include "ran.hpp"



struct Ranq2 {
    //Backup generator which has long period and fast speed. The period is about 8.5*(10^37)
        Ullong v,w;
    Ranq2(Ullong j) : v(4101842887655102017LL), w(1) {
        v ^= j;
        w = int64();
        v = int64();
    }
    inline Ullong int64() {
        v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
        w = 4294957665U*(w & 0xffffffff) + (w >> 32);
        return v ^ w;
    }
    inline Doub doub() { return 5.42101086242752217E-20* 1.273239545*int64(); }
    //Return random double-precision floating value in the Range 0 to 4/pi.
    inline Doub doub2() { return 5.42101086242752217E-20* 0.63661977*int64(); }
    //Return random double-precision floating value in the Range 0 to 2/pi.
    inline Uint int32() { return (Uint)int64(); }
    //Return 32-bit random integar.
};


struct Sinedev : Ranq2 {
    //Structure for sine deviates.
    Sinedev(Ullong i) : Ranq2(i){} 
    //Constructor arguments a random sequence seed.
    Doub dev() {
       // Return an exponential deviate.
        Doub u;
        do u =acos(1-1.57079633*doub());while (u==0);
 
        return u;
    } };
   
  int main(){
  
    std::clock_t start;
    double duration;
  
    fstream f1;
    int n,m,k,count;
    double random[100000][2];
    double j=0.01;
    double y;
    int bin[315];
    Ranq2 f(3);
    Sinedev g(6);//seed is 6
    
    
    f1.open("Q3.txt",ios::out);
    start=std::clock();
    for (n=0;n<100000;n++){
        random[n][1]=g.dev();
        random[n][2]=f.doub2();
        
        y=0.63661977*sin(random[n][1])*sin(random[n][1]);
     if(random[n][2]<=y){

        k=0;
        m=0;
        
    while (k==0){
     
    if (random[n][1]>=j*m && random[n][1]<j*(m+1)){
        bin[m]++;

        k=1;
    }
     else {
     m++;
     }
}
    }
    else {}
    
  }  
    for(m=0;m<315;m++){
       f1<<m*j<<"-"<<(m+1)*j<<"   "<<bin[m]<<endl;
       }
    
    
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"Time duration: "<< duration <<'\n';
    f1.close ();
    return 0;
    
} 

    
    