//
//  ran.cpp
//  
//
//  Created by Gu, Fangyuan on 26/11/2015.
//
//

#include "ran.hpp"


struct Ranq2 {
    //Backup generator which has long period and fast speed. The period is about 8.5*(10^37)
        Ullong v,w;
    Ranq2(Ullong j) : v(4101842887655102017LL), w(1) {
        v ^= j;
        w = int64();
        v = int64();
    }
    inline Ullong int64() {
        v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
        w = 4294957665U*(w & 0xffffffff) + (w >> 32);
        return v ^ w;
    }
    inline Doub doub() { return 5.42101086242752217E-20 * int64(); }
    //Return random double-precision floating value in the Range 0 to 1.
    inline Uint int32() { return (Uint)int64(); }
    //Return 32-bit random integar.
};


int main(){
    fstream f1;
    int i,n,m,k,count;
    double random[100000];
    double j=0.01;
    int bin[100];
    Ranq2 f(6);
    //void Ranq2::doub()
    
    f1.open("output_random.txt",ios::out);
    for (n=0;n<100000;n++){
        random[n]=f.doub();
       // f1<<random[n]<<endl;
        k=0;
        m=0;
        
    while (k==0){
     
    if (random[n]>=j*m && random[n]<=j*(m+1)){
        bin[m]++;

        k=1;
    }
     else {
     m++;
     }
}
    
  }  
    //cout<<bin[3]<<bin[5]<<"\n";
    for(m=0;m<100;m++){
       f1<<m*j<<"-"<<(m+1)*j<<"   "<<bin[m]<<endl;
       }

    f1.close ();
    return 0;
    
}