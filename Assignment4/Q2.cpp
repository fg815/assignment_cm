#include "ran.hpp"



struct Ranq2 {
    //Backup generator which has long period and fast speed. The period is about 8.5*(10^37)
        Ullong v,w;
    Ranq2(Ullong j) : v(4101842887655102017LL), w(1) {
        v ^= j;
        w = int64();
        v = int64();
    }
    inline Ullong int64() {
        v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
        w = 4294957665U*(w & 0xffffffff) + (w >> 32);
        return v ^ w;
    }
    inline Doub doub() { return 5.42101086242752217E-20* int64(); }
    //Return random double-precision floating value in the Range 0 to 1.
    inline Uint int32() { return (Uint)int64(); }
    //Return 32-bit random integar.
};


struct Sinedev : Ranq2 {
    //Structure for sine deviates.
    Sinedev(Ullong i) : Ranq2(i){} 
    //Constructor arguments a random sequence seed.
    Doub dev() {
       // Return an exponential deviate.
        Doub u;
        do u =acos(1-2*doub());while (u==0);

        return u;
    } };
   
  int main(){
  
    std::clock_t start;
    double duration;
    
    fstream f1;
    int n,m,k,count;
    double random[100000];
    double j=0.01;
    int bin[315];
    Sinedev g(3);//seed is 3
    
    f1.open("Q2.txt",ios::out);
    start=std::clock();
    for (n=0;n<100000;n++){
        random[n]=g.dev();
      
        k=0;
        m=0;
        
    while (k==0){
     
    if (random[n]>=j*m && random[n]<j*(m+1)){
        bin[m]++;

        k=1;
    }
     else {
     m++;
     }
}
    
  }  
    for(m=0;m<315;m++){
       f1<<m*j<<"-"<<(m+1)*j<<"   "<<bin[m]<<endl;
       }

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"Time duration: "<< duration <<'\n';
    f1.close ();
    return 0;
    
} 

    
    