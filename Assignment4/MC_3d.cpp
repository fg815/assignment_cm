#include"MC.hpp"

struct Ranq2 {
    //Backup generator which has long period and fast speed. The period is about 8.5*(10^37)
    Ullong v,w;
    Ranq2(Ullong j) : v(4101842887655102017LL), w(1) {
        v ^= j;
        w = int64();
        v = int64();
    }
    inline Ullong int64() {
        v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
        w = 4294957665U*(w & 0xffffffff) + (w >> 32);
        return v ^ w;
    }
    inline Doub doub() { return 5.42101086242752217E-20 * int64(); }
    //Return random double-precision floating value in the Range 0 to 1.
    inline Uint int32() { return (Uint)int64(); }
    //Return 32-bit random integar.
};

struct Sinedev : Ranq2 {
        //Structure for sine deviates.
    Sinedev(Ullong i) : Ranq2(i){} 
    //Constructor arguments a random sequence seed.
    Doub dev() {
        // Return an exponential deviate.
         Doub u;
         do u =acos(1-1.57079633*doub());
        while (u==0);
                                   
        return u;
    } };


double function(int di, double x, double y, double z){//define the function
    double f = sin(x)+sin(y)+sin(z);
    return f;
}

double weight(int di, double x, double y, double z){
    Sinedev g(6);
    double ran1=g.dev(), ran2=g.dev(), ran3=g.dev();
    return 0.5*(sin(ran1)+sin(ran2)+sin(ran3));
}

double Accuracy(){
    return 0.001;
}

int main(){
    double ran, x,y,z;
    double sum = 0.0;
    double avgValue[1000],ans[1000];
    int dimen = 3;//define the dimension
    double d[3];//maximum dimensions 10
    double b[3][2]={{0,PI},{0,PI},{0,PI}};//{{0,1},{2,3}}
    
    double (*func)(int,double,double,double)= function;//Function Pointer
    double (*weight)(int,double,double,double)= weight;//Weight Function Pointer
    
    int n=40;//Set the initial total number of random numbers.
    double e=Accuracy();//Set user defined accuracy:
    Sinedev g(5);// seed is 5
    
    for (int j = 1; j <= 2; j++)
    {
        for (int i = 1; i <= j*n; i++)
        {
            for(int k=1; k<=dimen; k++)
            {
            ran = g.dev();
            x = b[k][0] + (b[k][1] - b[k][0])*ran;
            y=0;
            z=0;
            }
            sum += (func)(dimen,x, y, z)/(weight)(dimen,x, y,z);
        }
        avgValue[j] = sum/(j*n);
        ans[j] = 3*avgValue[j];//normalized weighted function
        sum = 0.0;//restore sum
    }
    
    double err = fabs((ans[2]-ans[1])/ans[2]);
    
    int m=2;
    n = 2*n;
    sum =0.0;
    
    while(err>e){
        n = 2*n;
        m++;
        for (int i = 1; i <= n; i++)
        {
           for(int k=1; k<=dimen; k++)
            {
            ran = g.dev();
            x = b[k][0] + (b[k][1] - b[k][0])*ran;
            y=0;
            z=0;
            }
            sum += (func)(dimen,x,y,z)/(weight)(dimen,x,y,z);
           
        }
        avgValue[m] = sum/n;
        ans[m] = 3*avgValue[m];//Normalized weighted function
        err = fabs((ans[m]-ans[m-1])/ans[m]);
        sum = 0.0;
        }
        
        
        double I = 0.5*(ans[m]+ ans[m-1]);
        
        cout<<"Total number of points used: "<<n<<endl;
        cout<<"The integral of f(x) between a and b is: "<<I<<endl;
        
}
