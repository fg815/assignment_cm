//
//  ran.hpp
//  
//
//  Created by Gu, Fangyuan on 26/11/2015.
//
//

#ifndef ran_hpp
#define ran_hpp

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

//structdef Ranq2
typedef int Int; // 32 bit integer
typedef unsigned int Uint;

typedef long long int Llong; // 64 bit integer
typedef unsigned long long int Ullong;

typedef double Doub; // default floating type
typedef long double Ldoub;

#endif /* ran_hpp */


