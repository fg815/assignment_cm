//
//  Q2_a.cpp
//  
//
//  Created by Gu, Fangyuan on 06/02/2016.
//
//

#include "Q1.hpp"

int count=0;
fstream f3;

//define the differential equation for erf
double f1(double x, double y){
    return exp(-x*x);
}

double RK4(double (*f)(double,double), 
                        double x, double h,//x value and the stepsize h
                        double *y){//values of y
     
       double k1,k2,k3,k4;
       
       k1 = h*f(x,*y);
       k2 = h*f(x+0.5*h,*y+0.5*k1);
       k3 = h*f(x+0.5*h,*y+0.5*k2);
       k4 = h*f(x+h,*y+k3);
            
       *y = *y+(k1+2*k2+2*k3+k4)/6;

    }
    
int main(){
   
    double xini, xend;
    xini = 0;//set the domain of variable x
    xend = 2;
    double x=xini;//set the x to the initial value
    double err=0.000001;//delta0: value of accuracy
    double h=xend-x;//initial stepsize
    
    f3.open("dataQ2.txt",ios::out);
    //input the boundary value and the stepsize h
    double y,ya,yb;
    double delta_a=1.0;
    double delta, h_used;
    y = 0;//preset the initial boundary
    f3<<"value of x "<<"value of y "<<endl;
    f3<<x<<"  ,  "<<y<<endl;
    
    while(x<xend){//cover all the interval
           h = xend-x;//first set the step as the whole interval
           if(xend-x>1)
           {
           h=1;
           }//to prevent the interval to be too big and take too much calculation
           
           //set an initial value for delta to ensure it goes into while loop
           delta_a=1.0;
           
           while(delta_a > err*y) {
            //test the relative accuracy of y is larger than the epsilon§
            
               //initialize the y value
               ya = yb = y;
               
               RK4(f1, x, h, &ya);//do RK4 for a whole step
               
               h = 0.5*h;//half the stepsize h and decide the y1, y2 by two half steps
               
               RK4(f1, x, h, &yb);
               RK4(f1, x+h, h, &yb);
               //then calculate delta for y1 and y2 and compare, select the larger one to set the error delta
               
               delta_a=fabs(double (ya-yb));//evaluate error for y
               //cout<<delta_a<<"  "<<delta_b<<endl;
               //select the larger error as the delta of this step

               h_used=2*h;
               //new h is defined as:  
               h = 0.9*2*h*pow(err*y/delta_a, 1/5);//where the 0.9 is the safty coefficient
         
           }//then reiterate the step
           
           //h = 0.9*h*pow(err/delta, 1/6);//The new stepsize h
           
           y = yb+delta_a/15;
          
           x=x+h_used;//increment of x until it reach xend.
           
           f3<<x<<"  ,  "<<y<<endl;
           
           
           count++;//count the point used totally
           
        }
 f3<<"The total number of points used is:"<<count<<endl;
 f3<<"erf(2) = "<<2*y/sqrt(PI)<<endl;
 f3.close();
 return 0;
 
 }       
              
    
    
        