//
//  Q1.cpp
//  
//
//  Created by Gu, Fangyuan on 06/02/2016.
//
//

#include "Q1.hpp"


int count=0;
fstream f1;


//Function to evaluate the suitable stepsize of each step (double step)                               
double Adaptive2(double (*f)(double), double a, double b, double ep,                 
						   double S, double fa, double fb, double fc, int i) {                 
	//f1.open("Simpson.txt",ios::out);
	// The points are:a d c e b in order  
	double c = (a + b)/2, h = b - a;                                                                  
	double d = (a + c)/2, e = (c + b)/2;                                                              
	double fd = f(d), fe = f(e);                                                                      
	//fprintf(fnodes,"%.12e\n%.12e\n",d,e);
	count= count+2;
	double Sp = (h/12)*(fa + 4*fd + fc);  //evaluate the first half step                                                              
	double Sq = (h/12)*(fc + 4*fe + fb);  //evaluate the second half step                                                             
	double S2 = Sp + Sq;                                                                       
	if (i <= 0 || fabs(S2 - S) <= 15*ep) {
		if( i <=0 ) 
		cout<<"Reach the boundary"<<endl;//printf("bottom is reached\n");
		return S2 + (S2 - S)/15;     
	}
	return Adaptive2(f, a, c, ep/2, Sp,  fa, fc, fd, i-1) +                    
	Adaptive2(f, c, b, ep/2, Sq, fc, fb, fe, i-1);                     
}         
//If the accuracy is not reached, then half the stepsize and loop again.        

//
// Adaptive Simpson's Rule
//
double Adaptive1(double (*f)(double),   // point to function f
			double a, double b,  // // interval of x is: [a,b]
		        double ep,  // error tolerance
			int max) {   // number of subintervals into which [a,b] is to be divided    
	double c = (a + b)/2, h = b - a;                                                                  
	double fa = f(a), fb = f(b), fc = f(c);                                                           
	double S = (h/6)*(fa + 4*fc + fb); 
	//fprintf(fnodes,"%.12e\n",c);
	count++;
	return Adaptive2(f, a, b, ep, S, fa, fb, fc, max);                   
}                                                                                                   

//
// My function
//
double def(double x) {//define the erf function
	return exp(-x*x);
}

int main(){
        f1.open("Simpson_adaptive.txt",ios::out);
	double a=0.0, b=2.0;
	double tol=1.0e-6; // tolerance
	int j=100; // / maximum number of subintervals into which [a,b] is to be divided
	//fnodes=fopen("nodes.txt","w");
	//fprintf(fnodes,"%.12e\n%.12e\n",a,b);
	count=2;
	double I = Adaptive1(def, a, b, tol, j); // compute integral 
	// x is from 0 to 2 
	
	//printf("I = %.16e\n",I); // print the result: the integral I
	//printf("# of nodes = %i\n",count);
	//fclose(fnodes);
	f1<<"  Integral=  "<<2*I/sqrt(PI)<<endl;
	//printf("I = %.16e\n",I); // print the result
	f1<<"  Number of point used=  "<<count<<endl;

	f1.close();

	return 0;
}