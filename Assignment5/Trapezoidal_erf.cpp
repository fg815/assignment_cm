//
//  integral.cpp
//  
//
//  Created by Gu, Fangyuan on 04/12/2015.
//
//

#include "integral.hpp"

double F(double x){//define the function to integral
                   return exp(-x*x);
                  }

double Accuracy(){//let user specify the accuracy for integral
               
                  return 0.000001;
                 }


int main(){//integral function using Extended Trapezoidal Rule
    double a=0,b=2;//set the range of integration
    double e;
    double h[10000],I[10000];
    int n=2;
    e=Accuracy();//set user defined accuracy
    h[1]=b-a;
    h[2]=(b-a)/2;

    I[1]=h[1]*0.5*(F(a)+F(b));//first estimate
    I[2]=h[2]*(0.5*F(a)+F(a+h[2])+0.5*F(b));//update the estimate
    double err=fabs((I[2]-I[1])/I[1]);

    while(err>e){
        n=n+1;
        h[n]=(b-a)/n;
        int i=1;
        I[n]=h[n]*(0.5*F(a)+0.5*F(b));
        for(i=1;i<n;i++){
        I[n]=I[n]+h[n]*F(a+i*h[n]);
        }
        err=fabs((I[n]-I[n-1])/I[n-1]);
        }

    cout<<"Total number of points used: "<<n+1<<endl;
    cout<<"The integrated function erf(x) by Trapezoidal Rule is: "<<2/sqrt(PI)*I[n]<<endl;
}




            
        






