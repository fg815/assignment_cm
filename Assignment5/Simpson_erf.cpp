//
//  integral.cpp
//
//
//  Created by Gu, Fangyuan on 04/12/2015.
//
//

#include "integral.hpp"

double F(double x){//define the function to integral
    return exp(-x*x);
}

double Accuracy(){//let user specify the accuracy for integral
    
    return 0.000001;
}


int main(){//integral function using Extended Trapezoidal Rule
    double a=0,b=2;//set the range of integration
    double e,x;
    double h[10000],k[10000],I[10000];
    int n=2;
    e=Accuracy();//set user defined accuracy
    h[1]=(b-a)/2;
    h[2]=(b-a)/4;
    
    I[1]=h[1]/3*(F(a)+4*F(a+h[1])+F(b));//first estimate
    I[2]=h[2]/3*(F(a)+4*F(a+h[2])+2*F(a+2*h[2])+4*F(a+3*h[2])+F(b));//update the estimate
   
    double err=fabs((I[2]-I[1])/I[1]);
    cout<<err<<endl;
    
    while(err>e){
        n=n+1;
        h[n]=(b-a)/(2*n);
        k[n]=2*h[n];

        for(x=a;x<b;x=x+k[n]){//increment
            I[n]=I[n]+h[n]/3*(F(x)+4*F(x+h[n])+F(x+k[n]));
        }
        err=fabs((I[n]-I[n-1])/I[n-1]);
        //cout<<err<<endl;
    }
    
    cout<<"Total number of points used: "<<2*n+1<<endl;
    cout<<"The integrated function erf(x) by Simpson's Rule is: "<<2/sqrt(PI)*I[n]<<endl;
}







