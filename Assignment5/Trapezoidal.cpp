//
//  integral.cpp
//  
//
//  Created by Gu, Fangyuan on 04/12/2015.
//
//

#include "integral.hpp"

double F(double x){//define the function to integral
                   return x*x+2*x+28;
                  }

double Accuracy(){//let user specify the accuracy for integral
                double e;
                cout<<"Please specify the accuracy for integral(>0):"<<endl;
                cin>>e;
                while(e<=0){
                      cout<<"Please input a positive value"<<endl;
                      cout<<"Please specify the accuracy for integral(>0):"<<endl;
                      cin>>e;
                  }
                  return e;
                 }


int main(){//integral function using Extended Trapezoidal Rule
    double a=0,b=5;//set the range of integration
    double e;
    double h[10000],I[10000];
    int n=2;
    e=Accuracy();//set user defined accuracy
    h[1]=b-a;
    h[2]=(b-a)/2;

    I[1]=h[1]*0.5*(F(a)+F(b));//first estimate
    I[2]=h[2]*(0.5*F(a)+F(a+h[2])+0.5*F(b));//update the estimate
    double err=fabs((I[2]-I[1])/I[1]);

    while(err>e){
        n=n+1;
        h[n]=(b-a)/n;
        int i=1;
        I[n]=h[n]*(0.5*F(a)+0.5*F(b));
        for(i=1;i<n;i++){
        I[n]=I[n]+h[n]*F(a+i*h[n]);
        }
        err=fabs((I[n]-I[n-1])/I[n-1]);
        }

    cout<<"Total number of points used: "<<n+1<<endl;
    cout<<"The integral value obtained by Extended Trapezoidal Rule is: "<<I[n]<<endl;
}




            
        






