//
//  MCMC.cpp
//  
//
//  Created by Gu, Fangyuan on 01/03/2016.
//
//

#include "MCMC.hpp"
fstream f1, f2;

//define the Rosenbrock Test Function in 2D
double function(double x, double y){

	return (1-x)*(1-x)+100*(x*x-y)*(x*x-y)+10;//The value of this function is always larger than 0 
}//Since the minimum point is exactly 0, to avoid alpha=infinity, here can add a constant to the function and substrat this constant after find the minimum point.


//After burn in period, the convergence test of variance sigma^2 (for the last 20 points)
double Convergence(vector<double> x, vector<double> y, double *Ex, double *Ey){

	double avgx, avgy, sumx, sumy;
	sumx = 0;//initialize the sum
    sumy = 0;

    //Use the last 10 points inside the vector
	for(int i = x.size()-10; i < x.size(); ++i){//First calculate the average value
		sumx += x[i];
	}  
	avgx = sumx/10;

	*Ex=0;//Initialize standard deviation of x

       for(int i = x.size()-10; i < x.size(); ++i){//Then calculate the standard deviation

       	*Ex += (x[i] - avgx)*(x[i] - avgx);
       }

    *Ex = sqrt(*Ex/10);

//Then, do the same to calculate for y values:
       for(int i = y.size()-10; i < y.size(); ++i){//First calculate the average value
		sumy += y[i];
		
	   }  

	avgy = sumy/10;

	*Ey=0;//Initialize standard deviation of y

       for(int i = y.size()-10; i < y.size(); ++i){//Then calculate the standard deviation
       	*Ey += (y[i] - avgy)*(y[i] - avgy);
       }

       *Ey = sqrt(*Ey/(10));
       cout<<*Ey<<"  "<<*Ex<<endl;//Just for test
       //Update the standard deviation value of x and y
}



//Function to decide the mean value and the standard deviation of the new proposal function
double GaussianSet(double *mux, double *muy, double *sigmax, double *sigmay,
	                 vector<double> x, vector<double> y){
	 
         *mux = x.back();
         *muy = y.back();

	double accumx = 0.0;
	for(int i = 0; i < x.size(); ++i){//Then calculate the standard deviation
       	accumx += (x[i] - *mux)*(x[i] - *mux);
       }

        //*sigmax = sqrt( accumx / x.size());//The new standard deviation to be used in proposal function
        *sigmax=1;
// Now do the same to y points
         
	double accumy = 0.0;
	for(int j = 0; j < y.size(); ++j){//Then calculate the standard deviation
       	accumy += (y[j] - *muy)*(y[j] - *muy);
       }
       *sigmay = 1;
       //*sigmay = sqrt( accumy / y.size());//The new standard deviation to be used in proposal function
	//return Proposal(mux, muy, sigma, &newx, &newy);//Use the new Gaussian proposal functions
}



//Function to generate new (x, y) samples from proposal distributions (Gaussian here)
double Proposal(double mux, double muy, double Ex, double Ey,
	              double *newx, double *newy){
	
	//std::default_random_engine generator;
        std::random_device rd;
	    std::mt19937 generator(rd());
	    std::normal_distribution<double> distribution1(mux,Ex);
        std::normal_distribution<double> distribution2(muy,Ey);
    
       //double newx, newy;
       *newx = distribution1(generator);
       *newy = distribution2(generator);
       //cout<<*newx<<"   "<<*newy<<endl;//(To test the random number)
}



//Function for the Metropolis-Hasting step
double Metropolis(vector<double>* x, vector<double>* y, double newx, double newy,
	                 double mux, double muy, double Ex, double Ey, int *t){

	    double a,b;
	    double fa, fb;
	    std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<double> dis(0, 1);

        fa = function(x->back(), y->back());//The function value of the last point
        fb = function(newx, newy);//The function value of new point 
        
	    a = fb/fa;//Set alpha to the ratio of new function value and the old one
	    b = dis(gen);//Assign a random number to beta
	
	while(a > 1 && b > 1/a){//If a<=1 or b<=1/a, then the while loop ends and add the new x and new y to the end of chain
	
	        Proposal(mux, muy, Ex, Ey, &newx, &newy);
	        //cout<<newx<<"  "<<newy<<endl;
	        
	        fa = function(x->back(), y->back());//The function value of the last point
            fb = function(newx, newy);//The function value of new point 
        
	        a = fb/fa;//Set alpha to the ratio of new function value and the old one
	        b = dis(gen);//Assign a random number to beta
	
	        }
	        
	    x->push_back(newx);
		y->push_back(newy);

}

int main(){

	double newx, newy;
	int t=1;
	double mux, muy, sigmax, sigmay;//The mean value and the standard deviation of x and y respectively
    std::vector<double> x;
    std::vector<double> y;//Define the vector for x and y vector
    

	x.push_back(4);
	y.push_back(4);//Set the starting point to (4, 4), can be choose randomly

	mux = x.back();
	muy = y.back();//The initial Gaussian Proposal function 
        
	sigmax = 2;
	sigmay = 2;//Define the initial Gaussian, test and find suitable value for initial Gaussian function
	
	srand (time(NULL));
    std::default_random_engine generator(time(NULL));//put the seed into the two random generator: normal distribution and uniform distribution
        
	for(int i=0; i<=2000; i++){//The burn in period is set for 2000 points

		      
		      Proposal(mux, muy, sigmax, sigmay, &newx, &newy);//Generate newx, newy by updated Gaussian proposal function
		      //cout<<newx<<"  "<<newy<<endl;
		      //cout<<x.back()<<"  "<<y.back()<<endl;
		      Metropolis(&x, &y, newx, newy, mux, muy, sigmax, sigmay, &t);
		      
              //We have a new point add into the chain, then update the proposal Gaussian function,
              //Since it is still in the burn in period
                
              GaussianSet(&mux, &muy, &sigmax, &sigmay, x, y);
        
                
              //now the Gaussian is updated, loop again for generating new points

	}
	

    f1.open("data1.txt",ios::out);
    f2.open("data2.txt",ios::out);
    for(int i = 0; i < x.size(); ++i){//Print out all the points used in burn in period
    	f1<<x[i]<<"   ,   "<<y[i]<<endl;
	}  
    
    double Ex = 1;
	double Ey = 1;//Initilize the stardard deviation of the last 10 points;
    //Set to larger value 1.0 to make sure it can go into the while loop below:

	//After the burn in period, test the convergence
	x.erase(x.begin(), x.end()-10);//delete the previous points in the vector, except the last 10 points
	y.erase(y.begin(), y.end()-10);//The last 10 points are used for calculate the first few standard deviations of the chain

	Convergence(x, y, &Ex, &Ey);//Initialize the new standard deviation of the new chain, by the last 10 points in the burn in period

	double err= 0.001;//define accuracy for the standard devistion
	
	mux=x.back();
	muy=y.back();
	
	while ((Ex > err) || (Ey > err)) {//Keep generate new points when the chain is not converge.

		Proposal(mux, muy, Ex, Ey, &newx, &newy);
		//Here the Gaussian is not changed, mux, muy, sigmax and sigmay will not change any more.
		Metropolis( &x, &y, newx, newy, mux, muy, Ex, Ey, &t);
        //Add new point to the chain by Metropolis-Hasting step
        GaussianSet(&mux, &muy, &Ex, &Ey, x, y);//Add the function to adjust Gaussian proposal when the Gaussian is not fixed after the burn in period

        Convergence( x, y, &Ex, &Ey);
        //Calculate the new standard deviation of the last 10 points in the chain and compare with the precision 0.001
        
	}

	for(int j = 10; j < x.size(); ++j){//Print all the points used after the burn in period, start from the 10th point
    	f2<<x[j]<<"   ,   "<<y[j]<<endl;
    }


	//Satisfy the convergence criteria
	cout<<"The minimum point is at:"<<endl;
	cout<<x.back()<<"   "<<y.back()<<endl;
	double final = function(x.back(), y.back())-10;//minimum value of function calculated
	//-10 here is to substract the 10 added to the original function
	
	cout<<"The minimum value is:"<<endl;
	cout<<final<<endl;


    f1.close();
    f2.close();

}