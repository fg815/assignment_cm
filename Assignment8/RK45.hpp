//
//  Q1.hpp
//  
//
//  Created by Gu, Fangyuan on 06/02/2016.
//
//

#ifndef Q1_hpp
#define Q1_hpp

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

const int no=10000000;
const double PI =3.141592653589793238463;

int count=0;
fstream f3;


//define the 2 differential equations
double f1(double x, double y1, double y2){
    return -y1*y1-y2;
}

double f2(double x, double y1, double y2){
    return 5*y1-y2;
}

double RK4(double (*f)(double,double,double), double (*g)(double,double,double), 
                        double x, double h,//x value and the stepsize h
                        double *y1, double *y2){//values of y1 and y2
     
       double k1,k2,k3,k4,g1,g2,g3,g4;
       k1 = h*f(x,*y1,*y2);
       g1 = h*g(x,*y1,*y2);
       k2 = h*f(x+0.5*h,*y1+0.5*k1,*y2+0.5*g1);
       g2 = h*g(x+0.5*h,*y1+0.5*k1,*y2+0.5*g1);
       k3 = h*f(x+0.5*h,*y1+0.5*k2,*y2+0.5*g2);
       g3 = h*g(x+0.5*h,*y1+0.5*k2,*y2+0.5*g2);
       k4 = h*f(x+h,*y1+k3,*y2+g3);
       g4 = h*g(x+h,*y1+k3,*y2+g3);
            
       *y1 = *y1+(k1+2*k2+2*k3+k4)/6;
       *y2 = *y2+(g1+2*g2+2*g3+g4)/6;

    }



void finaldata(double y2){//print the data x, y1, y2 for final decided y2(0)
   
    double xini, xend;
    xini = 0;//set the domain of variable x
    xend = 10;
    double x=xini;//set the x to the initial value
    double err=0.000001;//delta0: value of accuracy
    double h=xend-x;//initial stepsize
    
    f3.open("data.txt",ios::out);
    //input the boundary value and the stepsize h
    double y1;
    double y1a,y1b,y2a,y2b;
    double delta_a=1.0, delta_b=1.0;//set an initial value for y1 and y2 to ensure it goes into while loop
    double delta, h_used;
    y1 = 1.5;//preset the initial boundary for y1(0)
   // y2 = 1.5; already defined in the function
    f3<<"value of x "<<"value of y1 "<<"value of y2 "<<endl;
    f3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
    
    
    while(x<xend){//cover all the interval
           h = xend-x;//first set the step as the whole interval
           if(xend-x>1)
           {
           h=1;
           }//to prevent the interval to be too big and take too much calculation
           
           
           delta_a=1.0;
           delta_b=1.0;
           while((delta_a > err) || (delta_b > err)) {
            //test the absolute error of y1 or y2 is larger than the epsilon§
            
               //initialize the y value
               y1a = y1b = y1;
               y2a = y2b = y2;
          
               RK4(f1, f2, x, h, &y1a, &y2a);//do RK4 for a whole step
               
               h = 0.5*h;//half the stepsize h and decide the y1, y2 by two half steps
               
               RK4(f1, f2, x, h, &y1b, &y2b);
               RK4(f1, f2, x+h, h, &y1b, &y2b);
               //then calculate delta for y1 and y2 and compare, select the larger one to set the error delta
               
               delta_a=fabs(double (y1a-y1b));//evaluate error for y1
               delta_b=fabs(double (y2a-y2b));//evaluate error for y2
               //cout<<delta_a<<"  "<<delta_b<<endl;
               //select the larger error as the delta of this step
               if(delta_a >= delta_b){
                      delta=delta_a;}
                      else{ delta=delta_b; }
               h_used=2*h;
               //new h is defined as:  
               h = 0.9*2*h*pow(err/delta, 1/5);//where the 0.9 is the safty coefficient
         
           }//then reiterate the step
           
           //h = 0.9*h*pow(err/delta, 1/6);//The new stepsize h
           
           y1 = y1b+delta_a/15;
           y2 = y2b+delta_b/15;
           x=x+h_used;//increment of x until it reach xend.
           
           f3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
           
           
           count++;//count the point used totally
           
        }
 f3<<"The total number of points used is:"<<count<<endl;
 f3.close();
 
 }  
 
#endif /* Q1_hpp */
