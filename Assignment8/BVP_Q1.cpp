//
//  BVP_Q1.cpp
//  
//
//  Created by Gu, Fangyuan on 06/02/2016.
//
//

#include "RK45.hpp"
#include <iostream>
#include "Brent.hpp"

double getvalue(double y2){//calulate the y2(10) for a guess of y2(0)
  //similar to finaldata function, but this just return the calculated y2(10), without print the data out in the txt file
  double xini, xend;
  xini = 0;//set the domain of variable x
  xend = 10;
  double x=xini;//set the x to the initial value
  double err=0.000001;//delta0: value of accuracy
  double h=xend-x;//initial stepsize


  double y1;
  double y1a,y1b,y2a,y2b;
  double delta_a=1.0, delta_b=1.0;//set an initial value for y1 and y2 to ensure it goes into while loo
  double delta, h_used;
  y1 = 1.5;//preset the initial boundary for y1(0)


    while(x<xend){//cover all the interval
           h = xend-x;//first set the step as the whole interval
           if(xend-x>1)
           {
           h=1;
           }//to prevent the interval to be too big and take too much calculation
           
           
           delta_a=1.0;
           delta_b=1.0;
           while((delta_a > err) || (delta_b > err)) {
            //test the absolute error of y1 or y2 is larger than the epsilon§
            
               //initialize the y value
               y1a = y1b = y1;
               y2a = y2b = y2;
          
               RK4(f1, f2, x, h, &y1a, &y2a);//do RK4 for a whole step
               
               h = 0.5*h;//half the stepsize h and decide the y1, y2 by two half steps
               
               RK4(f1, f2, x, h, &y1b, &y2b);
               RK4(f1, f2, x+h, h, &y1b, &y2b);
               //then calculate delta for y1 and y2 and compare, select the larger one to set the error delta
               
               delta_a=fabs(double (y1a-y1b));//evaluate error for y1
               delta_b=fabs(double (y2a-y2b));//evaluate error for y2
               //cout<<delta_a<<"  "<<delta_b<<endl;
               //select the larger error as the delta of this step
               if(delta_a >= delta_b){
                      delta=delta_a;}
                      else{ delta=delta_b; }
               h_used=2*h;
               //new h is defined as:  
               h = 0.9*2*h*pow(err/delta, 1/5);//where the 0.9 is the safty coefficient
         
           }//then reiterate the step
           
           //h = 0.9*h*pow(err/delta, 1/6);//The new stepsize h
           
           y1 = y1b+delta_a/15;
           y2 = y2b+delta_b/15;
           x=x+h_used;//increment of x until it reach xend.
           
        }
        cout<<y2<<endl;
 
 return y1-0;//return the funcion f=y1^(10)-y1(10), then use brent's method to find the root of f
 // As given, y1(10)=0
 
 }  
 
     
int main() {
    // Example function to find root of:
    /*auto f = [](double x) {
        return getvalue(x)-0;       
    };*/

    double y2min = -10;  // lower bound of search interval of y2(0)
    double y2max = 2;  // upper bound of search interval of y2(0)
    double tolerance = 0.000001;  // desired solution tolerance
    double max_iterations = 500;

    double root = brents_method(getvalue, y2min, y2max, tolerance, max_iterations);

    std::cout << "Root is: " << root << std::endl;  // produces "Root is: -1"
    // root is the final decided y2(0)

    finaldata(root);

    return 0;
}
           
           