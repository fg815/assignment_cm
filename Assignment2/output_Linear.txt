    x values    Linear interpolated y values
    -2.1           0.012155
    -2.09           0.0138472
    -2.08           0.0155395
    -2.07           0.0172317
    -2.06           0.018924
    -2.05           0.0206162
    -2.04           0.0223085
    -2.03           0.0240007
    -2.02           0.025693
    -2.01           0.0273852
    -2           0.0290775
    -1.99           0.0307697
    -1.98           0.032462
    -1.97           0.0341542
    -1.96           0.0358464
    -1.95           0.0375387
    -1.94           0.0392309
    -1.93           0.0409232
    -1.92           0.0426154
    -1.91           0.0443077
    -1.9           0.0459999
    -1.89           0.0476922
    -1.88           0.0493844
    -1.87           0.0510767
    -1.86           0.0527689
    -1.85           0.0544612
    -1.84           0.0561534
    -1.83           0.0578456
    -1.82           0.0595379
    -1.81           0.0612301
    -1.8           0.0629224
    -1.79           0.0646146
    -1.78           0.0663069
    -1.77           0.0679991
    -1.76           0.0696914
    -1.75           0.0713836
    -1.74           0.0730759
    -1.73           0.0747681
    -1.72           0.0764604
    -1.71           0.0781526
    -1.7           0.0798448
    -1.69           0.0815371
    -1.68           0.0832293
    -1.67           0.0849216
    -1.66           0.0866138
    -1.65           0.0883061
    -1.64           0.0899983
    -1.63           0.0916906
    -1.62           0.0933828
    -1.61           0.0950751
    -1.6           0.0967673
    -1.59           0.0984596
    -1.58           0.100152
    -1.57           0.101844
    -1.56           0.103536
    -1.55           0.105229
    -1.54           0.106921
    -1.53           0.108613
    -1.52           0.110305
    -1.51           0.111998
    -1.5           0.11369
    -1.49           0.115382
    -1.48           0.117074
    -1.47           0.118767
    -1.46           0.120459
    -1.45           0.122151
    -1.44           0.126309
    -1.43           0.130467
    -1.42           0.134625
    -1.41           0.138783
    -1.4           0.142941
    -1.39           0.147099
    -1.38           0.151257
    -1.37           0.155414
    -1.36           0.159572
    -1.35           0.16373
    -1.34           0.167888
    -1.33           0.172046
    -1.32           0.176204
    -1.31           0.180362
    -1.3           0.18452
    -1.29           0.191577
    -1.28           0.198634
    -1.27           0.205691
    -1.26           0.212748
    -1.25           0.219805
    -1.24           0.226862
    -1.23           0.233919
    -1.22           0.240976
    -1.21           0.248033
    -1.2           0.25509
    -1.19           0.262147
    -1.18           0.269204
    -1.17           0.276261
    -1.16           0.283318
    -1.15           0.290375
    -1.14           0.297432
    -1.13           0.304489
    -1.12           0.311546
    -1.11           0.318603
    -1.1           0.32566
    -1.09           0.332717
    -1.08           0.339774
    -1.07           0.346831
    -1.06           0.353888
    -1.05           0.360945
    -1.04           0.368002
    -1.03           0.375059
    -1.02           0.382116
    -1.01           0.389173
    -1           0.39623
    -0.99           0.403287
    -0.98           0.410344
    -0.97           0.417401
    -0.96           0.424458
    -0.95           0.431515
    -0.94           0.438572
    -0.93           0.445629
    -0.92           0.452686
    -0.91           0.459743
    -0.9           0.4668
    -0.89           0.473857
    -0.88           0.480914
    -0.87           0.487971
    -0.86           0.495028
    -0.85           0.502085
    -0.84           0.509142
    -0.83           0.516199
    -0.82           0.523256
    -0.81           0.530313
    -0.8           0.53737
    -0.79           0.544427
    -0.78           0.551484
    -0.77           0.558541
    -0.76           0.565598
    -0.75           0.572654
    -0.74           0.579711
    -0.73           0.586768
    -0.72           0.593825
    -0.71           0.600882
    -0.7           0.607939
    -0.69           0.614996
    -0.68           0.622053
    -0.67           0.62911
    -0.66           0.636167
    -0.65           0.643224
    -0.64           0.650281
    -0.63           0.657338
    -0.62           0.664395
    -0.61           0.671452
    -0.6           0.678509
    -0.59           0.685566
    -0.58           0.692623
    -0.57           0.69968
    -0.56           0.706737
    -0.55           0.713794
    -0.54           0.720851
    -0.53           0.727908
    -0.52           0.734965
    -0.51           0.742022
    -0.5           0.749079
    -0.49           0.756136
    -0.48           0.763193
    -0.47           0.77025
    -0.46           0.777307
    -0.45           0.784364
    -0.44           0.791421
    -0.43           0.798478
    -0.42           0.805535
    -0.41           0.812592
    -0.4           0.819649
    -0.39           0.826706
    -0.38           0.833763
    -0.37           0.84082
    -0.36           0.847877
    -0.35           0.854934
    -0.34           0.861991
    -0.33           0.869048
    -0.32           0.876105
    -0.31           0.883162
    -0.3           0.890219
    -0.29           0.897276
    -0.28           0.904333
    -0.27           0.91139
    -0.26           0.918447
    -0.25           0.925504
    -0.24           0.932561
    -0.23           0.939618
    -0.22           0.946675
    -0.21           0.953732
    -0.2           0.960789
    -0.19           0.961764
    -0.18           0.96274
    -0.17           0.963715
    -0.16           0.96469
    -0.15           0.965666
    -0.14           0.966641
    -0.13           0.967617
    -0.12           0.968592
    -0.11           0.969567
    -0.1           0.970543
    -0.09           0.971518
    -0.08           0.972493
    -0.07           0.973469
    -0.06           0.974444
    -0.05           0.975419
    -0.04           0.976395
    -0.03           0.97737
    -0.02           0.978346
    -0.01           0.979321
    -5.79398e-16           0.980296
    0.01           0.981272
    0.02           0.982247
    0.03           0.983222
    0.04           0.984198
    0.05           0.985173
    0.06           0.986149
    0.07           0.987124
    0.08           0.988099
    0.09           0.989075
    0.1           0.99005
    0.11           0.98759
    0.12           0.98513
    0.13           0.982671
    0.14           0.980211
    0.15           0.977751
    0.16           0.970821
    0.17           0.963891
    0.18           0.956961
    0.19           0.95003
    0.2           0.9431
    0.21           0.93617
    0.22           0.92924
    0.23           0.92231
    0.24           0.91538
    0.25           0.90845
    0.26           0.901519
    0.27           0.894589
    0.28           0.887659
    0.29           0.880729
    0.3           0.873799
    0.31           0.866869
    0.32           0.859939
    0.33           0.853009
    0.34           0.846078
    0.35           0.839148
    0.36           0.832218
    0.37           0.825288
    0.38           0.818358
    0.39           0.811428
    0.4           0.804498
    0.41           0.797567
    0.42           0.790637
    0.43           0.783707
    0.44           0.776777
    0.45           0.769847
    0.46           0.762917
    0.47           0.755987
    0.48           0.749056
    0.49           0.742126
    0.5           0.735196
    0.51           0.728266
    0.52           0.721336
    0.53           0.714406
    0.54           0.707476
    0.55           0.700545
    0.56           0.693615
    0.57           0.686685
    0.58           0.679755
    0.59           0.672825
    0.6           0.665895
    0.61           0.658965
    0.62           0.652034
    0.63           0.645104
    0.64           0.638174
    0.65           0.631244
    0.66           0.624314
    0.67           0.617384
    0.68           0.610454
    0.69           0.603524
    0.7           0.596593
    0.71           0.589663
    0.72           0.582733
    0.73           0.575803
    0.74           0.568873
    0.75           0.561943
    0.76           0.555013
    0.77           0.548082
    0.78           0.541152
    0.79           0.534222
    0.8           0.527292
    0.81           0.519656
    0.82           0.512019
    0.83           0.504383
    0.84           0.496746
    0.85           0.48911
    0.86           0.481473
    0.87           0.473837
    0.88           0.4662
    0.89           0.458564
    0.9           0.450927
    0.91           0.443291
    0.92           0.435654
    0.93           0.428017
    0.94           0.420381
    0.95           0.412744
    0.96           0.405108
    0.97           0.397471
    0.98           0.389835
    0.99           0.382198
    1           0.374562
    1.01           0.366926
    1.02           0.359289
    1.03           0.351653
    1.04           0.344016
    1.05           0.33638
    1.06           0.328743
    1.07           0.321106
    1.08           0.31347
    1.09           0.305833
    1.1           0.298197
    1.11           0.293377
    1.12           0.288557
    1.13           0.283737
    1.14           0.278917
    1.15           0.274097
    1.16           0.269277
    1.17           0.264457
    1.18           0.259637
    1.19           0.254817
    1.2           0.249997
    1.21           0.245178
    1.22           0.240358
    1.23           0.235538
    1.24           0.230718
    1.25           0.225898
    1.26           0.221078
    1.27           0.216258
    1.28           0.211438
    1.29           0.206618
    1.3           0.201798
    1.31           0.196978
    1.32           0.192158
    1.33           0.187338
    1.34           0.182518
    1.35           0.177698
    1.36           0.172878
    1.37           0.168058
    1.38           0.163238
    1.39           0.158418
    1.4           0.153598
    1.41           0.148779
    1.42           0.143959
    1.43           0.139139
    1.44           0.134319
    1.45           0.129499
    1.46           0.124679
    1.47           0.119859
    1.48           0.115039
    1.49           0.110219
    1.5           0.105399
    1.51           0.104591
    1.52           0.103784
    1.53           0.102976
    1.54           0.102168
    1.55           0.10136
    1.56           0.100553
    1.57           0.0997449
    1.58           0.0989371
    1.59           0.0981294
    1.6           0.0973217
    1.61           0.0965139
    1.62           0.0957062
    1.63           0.0948985
    1.64           0.0940907
    1.65           0.093283
    1.66           0.0924753
    1.67           0.0916675
    1.68           0.0908598
    1.69           0.0900521
    1.7           0.0892443
    1.71           0.0884366
    1.72           0.0876289
    1.73           0.0868211
    1.74           0.0860134
    1.75           0.0852057
    1.76           0.0843979
    1.77           0.0835902
    1.78           0.0827825
    1.79           0.0819747
    1.8           0.081167
    1.81           0.0803593
    1.82           0.0795515
    1.83           0.0787438
    1.84           0.0779361
    1.85           0.0771283
    1.86           0.0763206
    1.87           0.0755129
    1.88           0.0747051
    1.89           0.0738974
    1.9           0.0730897
    1.91           0.0722819
    1.92           0.0714742
    1.93           0.0706665
    1.94           0.0698587
    1.95           0.069051
    1.96           0.0682433
    1.97           0.0674355
    1.98           0.0666278
    1.99           0.0658201
    2           0.0650123
    2.01           0.0642046
    2.02           0.0633969
    2.03           0.0625891
    2.04           0.0617814
    2.05           0.0609737
    2.06           0.0601659
    2.07           0.0593582
    2.08           0.0585505
    2.09           0.0577427
    2.1           0.056935
    2.11           0.0561273
    2.12           0.0553195
    2.13           0.0545118
    2.14           0.0537041
    2.15           0.0528963
    2.16           0.0520886
    2.17           0.0512809
    2.18           0.0504731
    2.19           0.0496654
    2.2           0.0488577
    2.21           0.0480499
    2.22           0.0472422
    2.23           0.0464345
    2.24           0.0456267
    2.25           0.044819
    2.26           0.0440113
    2.27           0.0432035
    2.28           0.0423958
    2.29           0.0415881
    2.3           0.0407803
    2.31           0.0399726
    2.32           0.0391649
    2.33           0.0383571
    2.34           0.0375494
    2.35           0.0367417
    2.36           0.0359339
    2.37           0.0351262
    2.38           0.0343185
    2.39           0.0335107
    2.4           0.032703
    2.41           0.0318953
    2.42           0.0310875
    2.43           0.0302798
    2.44           0.0294721
    2.45           0.0286643
    2.46           0.0278566
    2.47           0.0270489
    2.48           0.0262411
    2.49           0.0254334
    2.5           0.0246257
    2.51           0.0238179
    2.52           0.0230102
    2.53           0.0222025
    2.54           0.0213947
    2.55           0.020587
    2.56           0.0197793
    2.57           0.0189715
    2.58           0.0181638
    2.59           0.0173561
    2.6           0.0165483
    2.61           0.0157406
    2.62           0.0149329
    2.63           0.0141251
    2.64           0.0133174
    2.65           0.0125097
    2.66           0.0117019
    2.67           0.0108942
    2.68           0.0100865
    2.69           0.00927874
    2.7           0.008471
    2.71           0.00766327
    2.72           0.00685554
    2.73           0.0060478
    2.74           0.00524007
    2.75           0.00443234
    2.76           0.0036246
    2.77           0.00281687
    2.78           0.00200914
    2.79           0.0012014
    2.8           0.000393669
    2.81           0.000389738
    2.82           0.000385806
    2.83           0.000381875
    2.84           0.000377944
    2.85           0.000374012
    2.86           0.000370081
    2.87           0.00036615
    2.88           0.000362218
    2.89           0.000358287
    2.9           0.000354356
    2.91           0.000350424
    2.92           0.000346493
    2.93           0.000342562
    2.94           0.00033863
    2.95           0.000334699
    2.96           0.000330768
    2.97           0.000326836
    2.98           0.000322905
    2.99           0.000318974
    3           0.000315042
    3.01           0.000311111
    3.02           0.00030718
    3.03           0.000303248
    3.04           0.000299317
    3.05           0.000295386
    3.06           0.000291454
    3.07           0.000287523
    3.08           0.000283592
    3.09           0.00027966
    3.1           0.000275729
    3.11           0.000271798
    3.12           0.000267866
    3.13           0.000263935
    3.14           0.000260004
    3.15           0.000256072
    3.16           0.000252141
    3.17           0.00024821
    3.18           0.000244278
    3.19           0.000240347
    3.2           0.000236416
    3.21           0.000232484
    3.22           0.000228553
    3.23           0.000224622
    3.24           0.00022069
    3.25           0.000216759
    3.26           0.000212828
    3.27           0.000208896
    3.28           0.000204965
    3.29           0.000201034
    3.3           0.000197102
    3.31           0.000193171
    3.32           0.00018924
    3.33           0.000185308
    3.34           0.000181377
    3.35           0.000177446
    3.36           0.000173514
    3.37           0.000169583
    3.38           0.000165652
    3.39           0.00016172
    3.4           0.000157789
    3.41           0.000153858
    3.42           0.000149926
    3.43           0.000145995
    3.44           0.000142064
    3.45           0.000138132
    3.46           0.000134201
    3.47           0.00013027
    3.48           0.000126338
    3.49           0.000122407
    3.5           0.000118476
    3.51           0.000114544
    3.52           0.000110613
    3.53           0.000106682
    3.54           0.00010275
    3.55           9.88189e-05
    3.56           9.48876e-05
    3.57           9.09562e-05
    3.58           8.70249e-05
    3.59           8.30936e-05
    3.6           7.91622e-05
    3.61           7.52309e-05
    3.62           7.12996e-05
    3.63           6.73682e-05
    3.64           6.34369e-05
    3.65           5.95056e-05
    3.66           5.55742e-05
    3.67           5.16429e-05
    3.68           4.77116e-05
    3.69           4.37802e-05
    3.7           3.98489e-05
    3.71           3.59175e-05
    3.72           3.19862e-05
    3.73           2.80549e-05
    3.74           2.41235e-05
    3.75           2.01922e-05
    3.76           1.62609e-05
    3.77           1.23295e-05
    3.78           8.3982e-06
    3.79           4.46687e-06
    3.8           5.35535e-07
