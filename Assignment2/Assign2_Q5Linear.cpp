#include <math.h> 
#include <stdio.h> 
#include <iostream> 
#include <stdlib.h> 
#include <fstream>
using namespace std;

//Linear interpolation fuction
double LinIntp(double u[], double v[],double xu,int s){ 
  return (xu-u[s])*(v[s+1]-v[s])/(u[s+1]-u[s])+v[s];
  //return (u[s+1]-xu)/(u[s+1]-u[s])*v[s]+(xu-u[s])/(u[s+1]-u[s])*v[s+1];
  //They have same interpolation value
}

int  main(){ 
  int i,k,m,s;
  const int max_arr_size=100;
  double x[max_arr_size],y[max_arr_size],yLint; 
  fstream f1,f2;
  //Read the data file
  f1.open("data1.txt", ios::in); 
 m=0; 
  while (!f1.eof()) { 
   m=m+1; 
   f1>>x[m]>>y[m];
   //For test: cout<<m<<"  "<<x[m]<<y[m]<<endl;
  } 
 f1.close(); 
 
 double xLint;

 f2.open("output_Linear.txt",ios::out);
 f2<<"    x values   "<<" Linear interpolated y values"<<endl;

 //set the input x from -2.1 to 3.8 with the stepwidth 0.01
 for(xLint=-2.10;xLint<3.80;xLint=xLint+0.01){
i = 1;
//Test which interval is the x value in
 while ((xLint-x[i])*(xLint-x[i+1])>0){ 
  i++;
 }
   k=i;
   // cout<<"k value is:"<<k<<endl;
   //Call the linear interpolation function and calculate y values:
 yLint=LinIntp(x,y,xLint,k);
 

 f2<<"    "<<xLint<<"           "<<yLint<<endl;
    
     }
 f2.close ();
 return 0;
}
