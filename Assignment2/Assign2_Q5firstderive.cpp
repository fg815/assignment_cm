#include <math.h> 
#include <stdio.h> 
#include <iostream> 
#include <stdlib.h> 
#include <fstream>
using namespace std;

//the function to calculate cubic interpolation y value:
double Cubintp(double x[],double y[], double y2[],double xu, int s){

double A=(x[s+1]-xu)/(x[s+1]-x[s]);
double B=(xu-x[s])/(x[s+1]-x[s]);
 double C=(A*A*A-A)*(x[s+1]-x[s])*(x[s+1]-x[s])/6;
 double D=(B*B*B-B)*(x[s+1]-x[s])*(x[s+1]-x[s])/6;
 // cout<<A<<" "<<B<<" "<<C<<"  "<<D<<" "<<s<<endl;
 // cout<<y2[s]<<"  "<<y2[s+1]<<endl;
 return A*y[s]+B*y[s+1]+C*y2[s]+D*y2[s+1];
  }

//matrix solve function A*u=F
double tridag(double a[], double b[], double c[], double r[], double u[],int n)
{
    int j;
    double bet;
    double gam[n];
    if (b[1] == 0.0) throw("Error 1 in tridag");
    u[1]=r[1]/(bet=b[1]);
    for (j=2;j<n;j++) {
        gam[j]=c[j-1]/bet;
        bet=b[j]-a[j]*gam[j];
        if (bet == 0.0) throw("Error 2 in tridag");
        u[j]=(r[j]-a[j]*u[j-1])/bet;
    }
    for (j=(n-2);j>=1;j--)
        u[j]-= gam[j+1]*u[j+1];
}

int  main(){ 
  int i,m,n,s,k;
  int j=2;
  const int max_arr_size=100;
  double yCub;
  double x[max_arr_size],y[max_arr_size];
  double a[11],b[11],c[11],y2[11],F[11]; 
  fstream f1,f2;
  //Read the data file
  f1.open("data1.txt", ios::in); 
  m=0; 
  while (!f1.eof()) { 
   m=m+1; 
   f1>>x[m]>>y[m];
   //For test: cout<<m<<"  "<<x[m]<<y[m]<<endl;
  } 
 f1.close();
 
 //set boundary condition to zero first derivative:
 b[1]=(x[2]-x[1])/3;
 c[1]=(x[2]-x[1])/6;
 a[m]=(x[m-1]-x[m])/6;
 b[m]=(x[m-1]-x[m])/3;
 F[1]=(y[2]-y[1])/(x[2]-x[1]);
 F[m]=(y[m]-y[m-1])/(x[m]-x[m-1]);
  
 //set the elements in tridiagnol matrix:
 for(j=2;j<m;j++){
 a[j]=(x[j]-x[j-1])/6;
 b[j]=(x[j+1]-x[j-1])/3;
 c[j]=(x[j+1]-x[j])/6;
 F[j]=(y[j+1]-y[j])/(x[j+1]-x[j])-(y[j]-y[j-1])/(x[j]-x[j-1]);
 //cout<<a[j]<<"  "<<b[j]<<"  "<<c[j]<<"  "<<F[j]<<endl;
 }

 //call tridiagonal function to solve matrix
 tridag(a, b, c, F, y2, m);
 for(n=1;n<=11;n++){

   // cout<<y2[n]<<endl;
 }
 
 //input the x values  
 double xCub;
 f2.open("output_firstderive.txt",ios::out);
 f2<<"    x values   "<<" Cubic interpolated y values(zero first derivative)"<<endl;
 
 for(xCub=-2.10;xCub<3.80;xCub=xCub+0.01){
 i =1;
//Test which interval is the x value in
 while ((xCub-x[i])*(xCub-x[i+1])>0){ 
  i++;
 }
   k=i;
   // cout<<"k value:"<<k<<endl;
   //Call the cubic interpolation function and calculate y values:
 yCub=Cubintp(x, y, y2, xCub, k);

  f2<<"    "<<xCub<<"           "<<yCub<<endl;
    
     }
 f2.close ();
 return 0;
}
