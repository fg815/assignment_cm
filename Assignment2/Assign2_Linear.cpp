#include <math.h> 
#include <stdio.h> 
#include <iostream> 
#include <stdlib.h> 
#include <fstream>
using namespace std;

//Linear interpolation fuction
double LinIntp(double u[], double v[],double xu,int s){ 
  return (xu-u[s])*(v[s+1]-v[s])/(u[s+1]-u[s])+v[s];
  //return (u[s+1]-xu)/(u[s+1]-u[s])*v[s]+(xu-u[s])/(u[s+1]-u[s])*v[s+1];
  //They have same interpolation value
}

int  main(){ 
  int i,k,m,s;
  const int max_arr_size=100;
  double x[max_arr_size],y[max_arr_size],yLint; 
  fstream f1;
  //Read the data file
  f1.open("data1.txt", ios::in); 
 m=0; 
  while (!f1.eof()) { 
   m=m+1; 
   f1>>x[m]>>y[m];
   //For test: cout<<m<<"  "<<x[m]<<y[m]<<endl;
  } 
 f1.close(); 
 
 double xint[4]={ 0.4, -0.128, -2.0, 3.2 };
 
 for(s=0;s<4;s++){
i = 1;
//Test which interval is the x value in
 while ((xint[s]-x[i])*(xint[s]-x[i+1])>0){ 
  i++;
 }
   k=i;
   //For test: cout<<"k value is:"<<k<<endl;
   //Call the linear interpolation function and calculate y values:
 yLint=LinIntp(x,y,xint[s],k); 
    cout<<"For x="<<xint[s]<<endl;
    cout<<"The linear interpolated y values are :"<<endl;  
    cout<<"      Linear:   "<<yLint<<endl;
    
     }
 return 0;
}
